```bash
git clone https://gitlab.com/JoaoVitorMendes/picom
cd picom
meson --buildtype=release . build
ninja -C build
# To install the binaries in /usr/local/bin (optional)
sudo ninja -C build install
```
```bash
picom --experimental-backends &
```
